<div class="showcase block block-border-bottom-grey">
  <div class="container">
	<h2 class="block-title">
	  $Title 
	</h2>
	$Content
	$Form
	$CommentsForm
  </div>
</div>